## Setup Instructions

- General note: This instruction file is written in **markdown** format. If you copying and pasting directory names from this file do not copy the backtick characters!

## Installing virtual environment

- From the top directory of the your copy of the PDAML repository run `source setup/nr-env.sh`
- This should take a couple of minutes to run
- You then have a virtual environment installed under `/scratch/nr-env`

### Running in the virtual environment

- Run `source /scratch/nr-env/bin/activate` to enter the virtual environment. You should get a prompt similar to the following:

```bash
(nr-env) -bash-4.2$
```

- Run the test script `python setup/setup-test.py` and confirm there are no errors (no output should be seen)

## Jupyter

- Launch Jupyter notebook (`jupyter-notebook`) from the prompt and open the notebook
- You should see a warning that a Python 3 kernel is being opened. Change the kernel to `nr-env` from the drop-down menu
- If you do not see this warning on the menu go to `Kernel > Change Kernel > nr-env"
- At this point you should see no warnings or errors for un-installed libraries

## Leaving the virtual environment

- To exit the virtual environment run `deactivate`

